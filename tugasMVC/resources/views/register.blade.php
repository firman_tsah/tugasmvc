<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form pendaftaran</title>
</head>
<body>
    <div class="judul">
        <h2>Buat Account Baru!</h2>
        <p><h3>Sign Up Form</h3></p>
    </div>

    <div class="form_isian">
        <form action ='/sapa' method="POST">
        @csrf
            <label for="nama_user">First name :</label> <br><br>
            <input type="text" placeholder="First name" id="nama_user">
            <br><br>
            <label for="nama_user">Last name :</label> <br><br>
            <input type="text" placeholder="Last name" id="nama_user">
        
    </div>

    <div class="gender"><br>
        <label for="">Gender :</label><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other
    </div>

    <div class="nationality"><br>
        <label for="">Nationality</label><br><br>
        <select name="" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
        </select>
    </div>

    <div class="Language"><br>
        <label for="">Language Spoken</label><br><br>
        <input type="checkbox" name="gender"> Bahasa Indonesia <br>
        <input type="checkbox" name="gender"> English <br>
        <input type="checkbox" name="gender"> Other
    </div><br>

    <div class="komentar">
        <label for="">Bio :</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        
    </div>
        <button type="submit"> Sign up</a></button>
    </form>    
</body>
</html>